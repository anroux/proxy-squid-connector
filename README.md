# Squid connector

This rewriter communicates with [proxy-filter-daemon](https://gitlab.esrf.fr/anroux/proxy-filter-daemon).
It decides if a request is allowed to pass or not based on the domain name.

## Behavior
In case of failure, the rewriter let requests pass through. More it fails, short will be the timeout.

## Squid configuration
```
url_rewrite_extras "%>a %>rm %{User-Agent}>h"
url_rewrite_program /path/to/squidrewriter.py
url_rewrite_children 4 startup=1 idle=1 concurrency=0
```

## Host configuration
The rewriter answers with `blacklisted-portal` when a request is blocked.
In order to make all this work, the `/etc/hosts` file must contains something like
```
127.0.0.1	localhost
127.0.10.1	blacklisted-portal
```
