#!/usr/bin/env python3
import sys
import time
import json
import socket
import threading

from urllib.parse import urlparse, unquote

CONTROL_SOCKET_PATH = '/var/run/proxy/control.sock'

class ControlLink(object):
    def __init__(self, socket_path):
        self.socket_path = socket_path
        self.first_fail = None

        try:
            self.renew_socket()
        except:
            # LOG HERE!
            pass

    def renew_socket(self):
        self.socket = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        self.socket.connect(self.socket_path)
        self.socket_file = self.socket.makefile()

    def kill(self):
        self.socket.shutdown(socket.SHUT_RDWR)
        self.socket.close()

    def try_request(self, request, default={}):
        retry = 5
        if self.first_fail is not None:
            since_first_fail = time.time() - self.first_fail
            retry = int(max(retry - since_first_fail / 5, 1))

        return self._try_request(request, default, retry=retry)

    def _try_request(self, request, default={}, retry=5):
        try:
            self.socket.sendall(request.encode('utf-8'))

            t = threading.Timer(1.0, self.kill)
            t.start()
            ans = self.socket_file.readline()
            
            t.cancel()
            self.first_fail = None

            try:
                return json.loads(ans)
            except:
                # LOG HERE!
                #sys.stderr.write('Fail to load JSON\n')
                return default

        except (BrokenPipeError, OSError):
            if self.first_fail is None:
                self.first_fail = time.time()

            if retry > 0:
                try:
                    self.renew_socket()
                except:
                    # LOG HERE!
                    time.sleep(.2)

                return self._try_request(request, default=default, retry=retry-1)

        # LOG HERE!
        #sys.stderr.write('Too many tries\n')
        return default

    def is_request_allowed(self, domain, path, info={}):
        if path is None or len(path) == 0:
            path = '-'

        request = 'GET request_allowance {} {} {}\n'.format(domain, path, json.dumps(info))
        ans = self.try_request(request, default={ 'allowed': True })

        if 'allowed' not in ans:
            # LOG HERE!
            #sys.stderr.write('Incorrect answer\n')
            return True

        #if not ans['allowed']:
        #    sys.stderr.write('{} blocked because {}\n'.format(domain, ans['reason']))

        return ans['allowed']

if __name__ == "__main__":
    link = ControlLink(CONTROL_SOCKET_PATH)

    for line in sys.stdin:
        try:
            user_request, source, method, _useragent = line.strip().split(' ', 3)
            useragent = unquote(_useragent)

            domain = None
            path = None
            if method == 'CONNECT':
                domain = user_request.split(':')[0]
            else:
                o = urlparse(user_request)
                domain = o.netloc.split(':')[0]
                path = o.path

            if link.is_request_allowed(domain, path, info={ 'source': source, 'useragent': useragent }):
                sys.stdout.write('OK\n')
            else:
                rewrite_url = 'http://blacklisted-portal/.well-known/portal/forbidden/'

                if method == 'CONNECT':
                    rewrite_url = 'blacklisted-portal:443'
                else:
                    if path.startswith('/.well-known/portal/'):
                        rewrite_url = 'http://blacklisted-portal{}'.format(path)
                    else:
                        rewrite_url = 'http://blacklisted-portal/.well-known/portal/forbidden/{}'.format(domain)

                sys.stdout.write('OK rewrite-url={}\n'.format(rewrite_url))

        except:
            #sys.stderr.write('Unexpected error\n')
            sys.stdout.write('OK\n')

        finally:
            sys.stdout.flush()